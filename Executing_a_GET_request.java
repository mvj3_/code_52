// To use these Internet methods, AndroidManifest.xml must have the following permission:
//<uses-permission android:name="android.permission.INTERNET"/>

URI myURI = null;
try {
	myURI = new URI("www.webserver.org");
} catch (URISyntaxException e) {
	// Deal with it
}
HttpClient httpClient = new DefaultHttpClient();
HttpGet getMethod = new HttpGet(myURI);
HttpResponse webServerResponse = null;
try {
	webServerResponse = httpClient.execute(getMethod);
} catch (ClientProtocolException e) {
	// Deal with it
} catch (IOException e) {
	// Deal with it
}

HttpEntity httpEntity = webServerResponse.getEntity();

if (httpEntity != null) {
	// You have your response; handle it. For instance, with an input
	// stream
	// InputStream input = entity.getContent();
}